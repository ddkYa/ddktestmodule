<?php
$vendor = "ddk";
$moduleName = "testmodule";

$paths = [
	$_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $vendor . "." . $moduleName . "/admin/options.php",
	$_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $vendor . "." . $moduleName . "/admin/options.php"
];

foreach ($paths as $path)
{
	if (is_file($path))
	{
		include_once($path);
		break;
	}
}