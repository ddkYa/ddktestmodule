<?php
#################################################
#        Company developer: DDK
#        Developer: Dmitry Kadrichev
#        Site:
#        E-mail: ddk-218@mail.ru
#################################################

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if (class_exists("ddk_testmodule"))
	return;

class ddk_testmodule extends CModule
{
	var $MODULE_ID = "ddk.testmodule";
	var $MODULE_NAME;
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "N";
	
	var $langPrefix;
	
	public function ddk_testmodule()
	{
		$arModuleVersion = [];
		include "version.php";
		
		$vendor = "ddk";
		$moduleName = "testmodule";
		$this->langPrefix = $vendor . "_" . $moduleName . "_";
		
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage($this->langPrefix . "INSTALL_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage($this->langPrefix . "INSTALL_DESCRIPTION");
		$this->PARTNER_NAME = $vendor;
	}
	
	public function DoInstall()
	{
		$this->InstallFiles();
		RegisterModule($this->MODULE_ID);
	}
	
	public function DoUninstall()
	{
		$this->UnInstallFiles();
		UnRegisterModule($this->MODULE_ID);
	}
	
	public function InstallFiles()
	{
		CopyDirFiles(
			__DIR__ . "/admin",
			$_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $this->MODULE_ID
		);
		
		return true;
	}
	
	public function UnInstallFiles()
	{
		DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $this->MODULE_ID);
		
		return true;
	}
}