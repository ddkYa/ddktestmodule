<?php

namespace DDK\TestModule\DB;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

//todo: написать языковой файл

/**
 * Class DemoTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(20) mandatory
 * <li> DESCRIPTION string optional
 * <li> DATE_INSERT date optional
 * </ul>
 *
 * @package Bitrix\Demo
 **/

class DemoTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'ddk_demo_table';
	}
	
	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('TABLE_ENTITY_ID_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('TABLE_ENTITY_NAME_FIELD'),
			),
			'DESCRIPTION' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('TABLE_ENTITY_DESCRIPTION_FIELD'),
			),
			'DATE_INSERT' => array(
				'data_type' => 'date',
				'title' => Loc::getMessage('TABLE_ENTITY_DATE_INSERT_FIELD'),
			),
		);
	}
	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 20),
		);
	}
}