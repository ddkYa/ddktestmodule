<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;
use DDK\TestModule\DB\DemoTable;

Loc::loadMessages(__FILE__);

$vendor = "ddk";
$moduleName = "testmodule";
$moduleId = $vendor . "." . $moduleName;
$langPrefix = $vendor . "_" . $moduleName . "_";

global $APPLICATION;
$APPLICATION->setTitle(Loc::getMessage($langPrefix . "OPTION_TITLE"));

$success = false;
if (CModule::includeModule($moduleId))
{
	$tableMap = DemoTable::getMap();
	$dbDemoTable = DemoTable::getList([]);
	
	$success = true;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<? if (!$success)
{
	echo "Module not installed";
}
else
{
	?>
    <table class="adm-list-table">
        <thead>
        <tr class="adm-list-table-header">
			<?
			foreach ($tableMap as $id => $call)
			{
				?>
                <td class="adm-list-table-cell">
                    <div class="adm-list-table-cell-inner"><?= $id ?></div>
                </td>
				<?
			}
			?>

        </tr>
        </thead>

        <tbody>
		<? while ($arDemoTable = $dbDemoTable->fetch())
		{
		?>
        <tr class="adm-list-table-row">
			<?
			foreach ($arDemoTable as $row)
			{
				?>
                <td class="adm-list-table-cell"><?= $row ?></td>
			<? } ?>
        <? } ?>
        </tr>
        </tbody>
    </table>
<? } ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>