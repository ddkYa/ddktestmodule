<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$vendor = "ddk";
$moduleName = "testmodule";
$moduleId = $vendor . "." . $moduleName;
$langPrefix = $vendor . "_" . $moduleName . "_";

$aMenu = [
	"parent_menu" => "global_menu_settings",
	"module_id" => $moduleId,
	"sort" => 40,
	"text" => Loc::getMessage($langPrefix . "MENU_NAME"),
	"icon" => "sys_menu_icon",
	"page_icon" => "sys_page_icon",
	"items_id" => "options",
	"url" => $moduleId . "/options.php"
];

return $aMenu;