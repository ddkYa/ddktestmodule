<?php

namespace Sprint\Migration;


class Version120180909134012 extends Version
{

    protected $description = "Install testModule table";
    protected $tableName = "ddk_demo_table";

    public function up() {
	    $connection = \Bitrix\Main\Application::getConnection();
	
	    ob_start();
	    ?>
	    CREATE TABLE IF NOT EXISTS <?=$this->tableName?>
	    (
	        ID int(11) NOT NULL auto_increment,
	        NAME CHAR(20) NOT NULL,
	        DESCRIPTION TEXT,
	        DATE_INSERT DATE,
	        PRIMARY KEY(ID)
	    );
	    <?
	    $sql = ob_get_clean();
	
	    $result = $connection->query($sql);
	
	    $this->outSuccess("Ready");
	
	    return;
    }

    public function down()
    {
	    $connection = \Bitrix\Main\Application::getConnection();
	
	    $sql = "DROP TABLE IF EXISTS ". $this->tableName .";";
	
	    $result = $connection->query($sql);
	
	    $this->outSuccess("Ready");
	    
	    return;
    }

}
